﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WeaponController : MonoBehaviour
{
    [Header("General")]
    public LayerMask hittableLayers;
    public GameObject bulletHolePrefab;
    public int contador = 0;

    [Header("Shoot Parameters")]
    public float FireRange = 200;
    public GameObject sound;
    //public float recoilForce = 4f; //Retroceso


    private Transform cameraPlayerTransform;

    private void Start()
    {
        cameraPlayerTransform = GameObject.FindGameObjectWithTag("MainCamera").transform;
    }

    private void Update()
    {
        HandleShoot();

    }
    public int level;

    public void HandleShoot()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Instantiate(sound);
            RaycastHit hit;
            if (Physics.Raycast(cameraPlayerTransform.position, cameraPlayerTransform.forward, out hit, FireRange, hittableLayers))
            {
                GameObject bulletHoleClone = Instantiate(bulletHolePrefab, hit.point + hit.normal * 0.001f, Quaternion.LookRotation(hit.normal));
                Destroy(bulletHoleClone, 4f);
                if (hit.collider.gameObject.CompareTag("Objetivo"))
                {
                    Destroy(hit.collider.gameObject);
                }
            }
        }
    }
}
