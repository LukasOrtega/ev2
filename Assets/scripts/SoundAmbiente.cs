﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundAmbiente : MonoBehaviour
{
    public GameObject sound;

    private void Start()
    {
        Instantiate(sound);
    }
}
