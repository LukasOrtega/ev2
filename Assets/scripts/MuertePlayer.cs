﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MuertePlayer : MonoBehaviour
{
    public int contador = 0;
    public int puntaje = 20;
   
   private void OnControllerColliderHit(ControllerColliderHit enemy)
    {
        if (enemy.collider.tag == "Objetivo")
        {
            contador++;
            GameControllerPuntos.Puntos -= puntaje ;       
            if(contador == 3)
            {
                SceneManager.LoadScene("GameOver");
                Cursor.lockState = CursorLockMode.None;
            }
        }

        if (enemy.collider.tag == "Finish")
        {
            SceneManager.LoadScene("GameOver");
            Cursor.lockState = CursorLockMode.None;
        }

       
    }

    

}
