﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Meta : MonoBehaviour
{

    private void Start()
    {
        
    }

    private void Update()
    {
        
    }

    private void OnControllerColliderHit(ControllerColliderHit meta)
    {
        if(meta.collider.tag == "Meta")
        {
            SceneManager.LoadScene("PantallaEstadisticas");
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
