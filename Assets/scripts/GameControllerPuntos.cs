﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameControllerPuntos : MonoBehaviour
{
    public static int Puntos = 0;
    public string PuntosString = "Puntaje: ";

    public Text Textpuntos;

    public static GameControllerPuntos gamecont;

    private void Awake()
    {
        gamecont = this;
    }

    
   
    void Update()
    {
        if (Textpuntos != null)
        {
            Textpuntos.text = PuntosString + Puntos.ToString();
        }
    }
}
